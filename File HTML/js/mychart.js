$.getJSON('sensor.json', function (json) {
    var suhu = json.map(function(item) {
        return item.temp;
    });
    var time = json.map(function(item) {
        return item.time;
    });
    var ctx = document.getElementById("myline1").getContext("2d");
    var mybar = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: time,
            datasets: [{
                label: "My First dataset",
                backgroundColor: "rgba(220,220,220,1)",
                borderColor: "rgba(220,200,230,1)",
                data: suhu,
                fill: false,
            }]
        },
        options:{
            scales: {
                yAxes : [{
                    ticks : {
                        max : 100,    
                        min : -20,
                        beginAtZero : true
                    }
                }]
            }
        }
    });
});

var ctx = document.getElementById("canvas").getContext("2d");
var mychart = {
    labels: ["January", "February", "March", "April", "May", "June", "July"],
    datasets: [{
        type: 'line',
        label: 'Dataset 1',
        borderColor: "rgba(220,10,10,1)",
        borderWidth: 2,
        fill: false,
        data: [10,15,44,33,50,12,40]
    }, {
        type: 'bar',
        label: 'Dataset 2',
        backgroundColor: "rgba(220,20,20,1)",
        data: [10,15,44,33,50,12,40],
        borderColor: 'white',
        borderWidth: 2
    }, {
        type: 'bar',
        label: 'Dataset 3',
        backgroundColor:"rgba(100,50,50,0.5)",
        data:[10,15,44,33,50,12,40]
    }]
};
window.myMixedChart = new Chart(ctx, {
    type: 'bar',
    data: mychart,
    options: {
        responsive: true,
        title: {
            display: true,
            text: 'Chart.js Combo Bar Line Chart'
        },
        tooltips: {
            mode: 'index',
            intersect: true
        },
        scales: {
            yAxes : [{
                ticks : {
                    max : 100,    
                    min : -20,
                    beginAtZero : true
                }
            }]
        }
    }
});