//const preobjek  = document.getElementById('average');
const dbref     = firebase.database().ref('sensor');

function suhuToArray(snapshot) {
    const returnArr = [];
    snapshot.forEach(function(childSnapshot) {
        const item = childSnapshot.val().temp;
        returnArr.push(item);
    });
    return returnArr;
};

function tanggalToArray(snapshot) {
    const returnArr = [];
    snapshot.forEach(function(childSnapshot) {
        const item = childSnapshot.val().date;
        returnArr.push(item);
    });
    return returnArr;
};

function jamToArray(snapshot) {
    const returnArr = [];
    snapshot.forEach(function(childSnapshot) {
        const item = childSnapshot.val().time;
        returnArr.push(item);
    });
    return returnArr;
};

firebase.database().ref('sensor').limitToLast(10).on('value', function(snapshot) {
    const suhu      = suhuToArray(snapshot);
    const jam       = jamToArray(snapshot);
    const tanggal   = tanggalToArray(snapshot);
    var ctx = document.getElementById("chartjs").getContext("2d");
    var mychart = {
        labels: jam,
        datasets: [{
            type: 'line',
            label: 'Dataset 1',
            borderColor: "rgba(220,10,10,1)",
            borderWidth: 2,
            fill: false,
            data: suhu
        }, {
            type: 'bar',
            label: 'Dataset 2',
            backgroundColor: "rgba(220,20,20,1)",
            data: suhu,
            borderColor: 'white',
            borderWidth: 2
        }, {
            type: 'bar',
            label: 'Dataset 3',
            backgroundColor:"rgba(100,50,50,0.5)",
            data: suhu
        }]
    };
    window.myMixedChart = new Chart(ctx, {
        type: 'bar',
        data: mychart,
        options: {
            responsive: true,
            title: {
                display: true,
                text: 'Chart.js Combo Bar Line Chart'
            },
            tooltips: {
                mode: 'index',
                intersect: true
            },
            scales: {
                yAxes : [{
                    ticks : {
                        max : 100,    
                        min : -20,
                        beginAtZero : true
                    }
                }]
            }
        }
    });
    ////////////////////
    var values = suhu;
    var sum = values.reduce((a, b) => a += b);
    var avg = sum / values.length;
    var two = avg.toFixed(2);
    document.getElementById('average').innerHTML = two;
});

window.addEventListener('load', function() {
    myAudio = new Audio('audio/alarm.mp3'); 
    myAudio.addEventListener('ended', function() {
      this.currentTime = 0;
    }, true);
    firebase.database().ref('sensor').limitToLast(1).on('child_added', function(data) {
        var sensor = data.val();
        if (sensor.temp > 40 ) {
            if (myAudio.paused || myAudio.ended) {
              myAudio.play()
            }
        }
        else if (sensor.temp < 40 ) {
            if (!myAudio.paused) {
              myAudio.pause();
            }
        }
    });
});
//dbref.on('value', snap => {
//  console.log(snap.val());
//});