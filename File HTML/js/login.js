(function(){
    "use strict";
    const txtemail      = document.getElementById('txtemail');
    const txtpass       = document.getElementById('txtpass');
    const btnlogin      = document.getElementById('btnlogin');
    const btnreg        = document.getElementById('btnreg');
    const btnout        = document.getElementById('btnout');
    const texthiden     = document.getElementById('txthide');

    btnlogin.addEventListener('click', e => {
        const email = txtemail.value;
        const pass  = txtpass.value;
        const auth  = firebase.auth();

        const promise = auth.signInWithEmailAndPassword(email, pass);
        promise.catch(e => console.log(e.message));
    });
    btnreg.addEventListener('click', e => {
        const email = txtemail.value;
        const pass  = txtpass.value;
        const auth  = firebase.auth();
        const promise = auth.createUserWithEmailAndPassword(email, pass);
        promise.catch(e => console.log(e.message));
    });
    firebase.auth().onAuthStateChanged(firebaseUser =>{
        if(firebaseUser){
            console.log(firebaseUser);
            btnout.classList.remove('hide');
            texthiden.ty
            //window.location = "ade.html";
        }else{
            console.log('not login');
            btnout.classList.add('hide');
        }
    });
    btnout.addEventListener('click', e => { 
        firebase.auth().signOut();
    }); 
}());