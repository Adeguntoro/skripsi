window.addEventListener('load', function() {
    firebase.database().ref('sensor').limitToLast(1).on('child_added', function(data) {
        var sensor = data.val();
        //console.log("suhu gauge is :"+sensor.temp)
        //if (sensor.temp < 50) {
        //    var audio = new Audio('audio/jingle-bells-sms.mp3');
        //    audio.play();
        //} else if (sensor.temp == 80) {
        //    var audio = new Audio('solemn.mp3');
        //    audio.play();
        //} else if (sensor.temp == 100) {
        if (sensor.temp > 40 ) {
            myAudio = new Audio('audio/alarm.mp3'); 
            myAudio.addEventListener('ended', function() {
                this.currentTime = 0;
                this.play();
                //this.pause();
            }, false);
            myAudio.play()
        }
        else if (sensor.temp < 40 ){
            myAudio = new Audio('audio/alarm.mp3'); 
            myAudio.addEventListener('ended', function() {
                this.currentTime = 0;
                this.pause();
            }, false);
        }
        mydata.setValue(0, 1, sensor.temp);
        chart.draw(mydata, myoptions);
        //console.log(sensor.temp);
    });
});
google.charts.load('current', {'packages':['gauge']});
google.charts.setOnLoadCallback(drawChart);
function drawChart() {
    mydata = google.visualization.arrayToDataTable([
        ['Label', 'Value'],
        ['Temp' , 0]
    ]);
    myoptions           = {
        width           : 300,
        height          : 300,
        max             : 100,
        min             : -10,
        minorTicks      : 5,
        majorTicks      : ['-10','-5','0','10','20','30','40','50','60','70','80','90','100']};
    chart               = new google.visualization.Gauge(document.getElementById('gauge'));
    chart.draw(mydata, myoptions);
}