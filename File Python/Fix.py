import pyrebase
from time import *
import datetime
import locale
from w1thermsensor import *
import smtplib
from email.mime.multipart import *
from email.mime.text import *

################### Firebase Initialize
config = {
  "apiKey"		    : "Your API KEY",
  "authDomain"		: "Your.firebaseapp.com", 
  "databaseURL"		: "https://Your.firebaseio.com",
  "storageBucket"	: "Your.appspot.com",
  "serviceAccount"  : "Your.json"
}
firebase 	= pyrebase.initialize_app(config)
db  		= firebase.database()
###################

###################
me      = "Your email"
you     = ["Receiver email","Receiver email","Receiver email"]
user    = "Your email"
pw      = "Your Password"
###################
msg = MIMEMultipart('alternative')
msg['Subject'] = "Peringatan Suhu Ruangan"
msg['From'] = me
msg['To'] = ",".join(you)
###################
while True:
    ########################## Menentukan waktu atau jam
    locale.setlocale(locale.LC_TIME, "id_ID")
    now     = datetime.datetime.now()
    date    = now.strftime("%A, %d/%m/%Y")
    hari    = now.strftime("&A")
    clock   = now.strftime("%H:%M:%S")
    try:
        ########################### Baca Suhu
        sensor1 = W1ThermSensor(W1ThermSensor.THERM_SENSOR_DS18B20, "Your sensor ID")
        hasil  = sensor1.get_temperature(unit=1)
        ########################### Upload data ke Firebase
        data   = {"temp": hasil, "date":date, "time":clock}
        db.child("sensor").push(data)
        ########################## Email Peringatan
        text = "Suhu Ruang server telah mencapai suhu {} Celsius, jam {}, hari dan tanggal {} WIB.".format(hasil, clock, date)
        part = MIMEText(text, 'plain')
        #msg.attach(part)
        s = smtplib.SMTP_SSL('smtp.gmail.com', 465)
        s.ehlo()
        #s.starttls()
        s.login(me,pw)
        if hasil > 27 : ######## Contoh
	    msg.attach(part)
            s.sendmail(me, you, msg.as_string())
            s.quit()
            print "Suhu ruangan diambang batas, {} celsius pada jam {} hari dan tanggal {}".format(hasil,clock,date)
            status = {"Kondisi": "Bahaya"}
            db.child("notif/status").set(status)
        else:
            print "Suhu Aman ", hasil
            status = {"Kondisi": "Normal"}
            db.child("notif/status").set(status)
    except KeyboardInterrupt :
	print "Pemantauan Dibatalkan!"
        pemantauan = {"Kondisi": "Dibatalkan"}
        db.child("notif/pemantauan").set(pemantauan)
        break
    except NoSensorFoundError :
        print "Sensor Suhu Tidak Ada"
        kondisi_sensor = {"Kondisi": "Sensor Tidak Ada"}
        db.child("notif/sensor").set(kondisi_sensor)
        break
    except SensorNotReadyError:
        print "Sensor Suhu Belum Siap"
        break
    except NameError :
        print "ada masalah"
        break
    sleep(60)
